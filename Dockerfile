FROM alpine:edge AS build
RUN apk add --no-cache --update go gcc g++
WORKDIR /app
ENV GOPATH /app
COPY * /app
RUN go get # testgo is name of our application
RUN CGO_ENABLED=1 GOOS=linux go build .

FROM alpine:edge
WORKDIR /app
# RUN cd /app
COPY --from=build /app/testgo /app/testgo
CMD ["/app/testgo"]
