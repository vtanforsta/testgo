```sh
eval $(minikube -p minikube docker-env); docker build . -t testgo:latest
minikube addons enable ingress
kubectl apply -f kube.yml
kubectl rollout restart deploy
```

```sh
minikube tunnel

curl localhost:80
curl localhost:80/create
curl localhost:80/list
```