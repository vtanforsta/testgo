package main

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

func main() {
	// ":80"
	addr := os.Getenv(`TESTGOADDR`)
	fmt.Println("starting on", addr)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// "http://localstack-service:4566"
	localstackAddr := os.Getenv(`LOCALSTACKADDR`)
	fmt.Println("localstack at", localstackAddr)

	cfg, err := config.LoadDefaultConfig(ctx,
		config.WithEndpointResolverWithOptions(aws.EndpointResolverWithOptionsFunc(func(service, region string, options ...interface{}) (aws.Endpoint, error) {
			return aws.Endpoint{
				URL: localstackAddr,
			}, nil
		})),
		config.WithRegion("us-east-1"),
	)
	cfg.Credentials = aws.NewCredentialsCache(credentials.NewStaticCredentialsProvider("test", "test", ""))
	if err != nil {
		panic(err)
	}

	http.HandleFunc("/", getRoot)
	http.HandleFunc("/create", createBucket(ctx, cfg))
	http.HandleFunc("/list", listBuckets(ctx, cfg))
	err = http.ListenAndServe(addr, http.DefaultServeMux)
	if err != nil {
		panic(err)
	}
}

func getRoot(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "time is %s\n", time.Now())
	fmt.Fprintf(w, "remote %s\n", r.RemoteAddr)
	hn := os.Getenv(`HOSTNAME`)
	fmt.Fprintf(w, "hostname %s\n", hn)
}

func createBucket(ctx context.Context, cfg aws.Config) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// Create S3 service client
		client := s3.NewFromConfig(cfg, func(o *s3.Options) {
			o.UsePathStyle = true
		})

		buck := fmt.Sprintf("bucket-%d", rand.Int())
		out, err := client.CreateBucket(ctx, &s3.CreateBucketInput{Bucket: aws.String(buck)})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintln(w, err)
			return
		}
		fmt.Fprintln(w, "create bucket", "err", err, "result", *out.Location)
	}
}

func listBuckets(ctx context.Context, cfg aws.Config) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// Create S3 service client
		client := s3.NewFromConfig(cfg, func(o *s3.Options) {
			o.UsePathStyle = true
		})

		out, err := client.ListBuckets(ctx, &s3.ListBucketsInput{})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintln(w, err)
			return
		}
		fmt.Fprintln(w, "list buckets", "err", err, "result", *out.Owner, *out.Owner.DisplayName, *out.Owner.ID)
		for _, b := range out.Buckets {
			fmt.Fprintln(w, "a bucket", *b.Name, *b.CreationDate)
		}
	}
}
